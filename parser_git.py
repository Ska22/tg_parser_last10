# класс для подключения учетной записи ТГ
from telethon.sync import TelegramClient
# класс для работы с сообщениями
from telethon.tl.functions.messages import GetHistoryRequest

api_id =   # ТГ id
api_hash = " " # ТГ токен
phone = '' # ТГ номер телефона

client = TelegramClient(phone, api_id, api_hash)
client.start()
channel = 'https://t.me/ozonmarketplace' # ТГ канал
total = 10 # количество последних записей


def rew(channel):
    spisok = client(
        GetHistoryRequest(peer=channel, offset_id=0, offset_date=None, add_offset=0, limit=1, max_id=0, min_id=0,
                          hash=0))
    for i in spisok.messages:
        i = i.to_dict()
        if i['id'] < 0:
            return "Пустая группа"
        else:
            return f"Всего {i['id']} постов. Некоторые посты могут быть удалены - но они все равно фиксируются в количестве id и выводится не будут"


print(rew(channel))


def message(chan, bo):
    spisok = client(
        GetHistoryRequest(peer=chan, offset_id=0, offset_date=None, add_offset=0, limit=bo, max_id=0, min_id=0,
                          hash=0))
    t_mes = []
    for i in spisok.messages:
        i = i.to_dict()
        if i['message'] == '':
            continue
        else:
            t_mes.append(i)
    if len(t_mes) < total:
        return message(channel, bo + 1)
    else:
        return t_mes


def name_channel(channel):
    return client.get_entity(channel).to_dict()['title']


try:
    for i in message(channel, total)[:total]:
        print(name_channel(channel) + ' ' + str(i['date']), i['message'][:(i['message'].find('\n'))])
except KeyError:
    print('Измените количество строк для парсинга/Либо в группе нет сообщений')
